# Maintainer: Yurii Kolesnykov <root@yurikoles.com>

pkgname=insync-emblem-icons
pkgver=3.8.1.50459
pkgrel=1
pkgdesc="File manager emblem icons for Insync file manager extensions"
url="https://www.insynchq.com/downloads"
license=('custom:insync')
options=(!strip)
arch=('any')
makedepends=('imagemagick')
_conflictver=3.4.1
conflicts=("insync-caja<=${_conflictver}"
        "insync-dolphin<=${_conflictver}"
        "insync-nautilus<=${_conflictver}"
        "insync-nemo<=${_conflictver}")

source=("${pkgname}-${pkgver}.deb::http://cdn.insynchq.com/builds/linux/${pkgname}_${pkgver}_all.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/filemanager.png")
sha256sums=('0f3ec658cc696faf262e78b380902ffb22a3d4726e8781b33582d5c8f1729225'
            '208544ce159af0c02821e2f1006d09875e9d9fa91be71114a2f9034f561bc42c'
            '3b19be9e841750dd69bc8bedb141dc74a9cac7ec6a885223cc5d1bf26af6d925')

_insync_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=insync start
TryExec=insync
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_insync_desktop" | tee com.${pkgname//-/_}.desktop
}

package() {
    tar xf data.tar.gz
    cp -rp usr "${pkgdir}"

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname//-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname//-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/filemanager.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
